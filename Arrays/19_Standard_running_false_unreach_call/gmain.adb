with Standard_running_false_unreach_call;
procedure Gmain with
  SPARK_Mode => on
is
begin
	Standard_running_false_unreach_call.Main; 
end Gmain;
