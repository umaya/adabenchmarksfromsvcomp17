with Ada.Text_IO; use Ada.Text_IO;

package body Standard_vector_difference_true_unreach_call_ground with
  SPARK_Mode => on
is
   procedure Main is
		size: Integer:= 10;
   		a: array (0..size) of Integer;
   		b: array (0..size) of Integer;
   		c: array (0..size) of Integer;
		i: Integer:=0; 
   begin   
  		while( i < size ) loop 
			c(i):= a(i) - b(i);
			i := i+1; 
		end loop;


		for x in Integer range 0..size-1 loop
			pragma Assert(c(x) = a(x) -b(x));
		end loop; 
   end Main;


end Standard_vector_difference_true_unreach_call_ground;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./standard_vector_difference_true_unreach_call_ground.gpr --prover=cvc4,z3,altergo --mode=all --report=all
