with Ada.Text_IO; use Ada.Text_IO;

package body Standard_strcpy_true_unreach_call_ground with
  SPARK_Mode => on
is
   subtype Rand_Range is Integer;
 

   procedure Main is
		N: Integer:= 100000;
   		src: array (0..N) of Integer;
   		dst: array (0..N) of Integer;
		i: Integer:=0;
		j: Integer:=0; 
   begin 
 

  		while( j < N ) loop 
			src(j) := 851984;
			j := j+1; 
		end loop;
 
  		while (i < N and src(i) /= 0) loop
    		dst(i) := src(i);
    		i := i + 1;
		end loop; 
 
 		for  x in Integer range 0..i-1 loop
   			pragma Assert(dst(x) = src(x));
		end loop;
       
   end Main;


end Standard_strcpy_true_unreach_call_ground;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./standard_strcpy_true_unreach_call_ground.gpr -k -U --mode=all --report=all
