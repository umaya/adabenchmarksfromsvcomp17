with Ada.Text_IO; use Ada.Text_IO;

package body Standard_copy1_false_unreach_call_ground with
  SPARK_Mode => on
is
  

   procedure Main is
		size: Integer:= 100000;
   		a1: array (0..size) of Integer;
   		a2: array (0..size) of Integer;
		i: Integer:=0;
		j: Integer:=0;
   begin  
		 
		for a in Integer range 0..size-1 loop 
			a1(a) := 200;
			a2(a) := -842548;
		end loop;

  		for a in Integer range 0..size-1 loop 
			a1(a) := a1(a);
		end loop;


  		for a in Integer range 0..size-1 loop 
			pragma Assert(a1(a) = a2(a) );
  		end loop; 
       
   end Main;


end Standard_copy1_false_unreach_call_ground;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./standard_copy1_false_unreach_call_ground.gpr -k -U --mode=all --report=all
