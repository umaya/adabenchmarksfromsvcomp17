with Ada.Text_IO; use Ada.Text_IO;

package body Standard_running_true_unreach_call with
  SPARK_Mode => on
is
   procedure Main is
		N: Integer:= 7;
   		a: array (0..N-1) of Integer;  
   		b: array (0..N-1) of Integer;  
		i : Integer:=0;
		f : Integer:=1;
   begin   
   
   		while (i < N) loop 
			if a(i) >= 0  then 
			   b(i) := 1;
			else 
			   b(i) := 0;
			end if;
			i := i + 1;
		end loop; 

 		i:= 0;
		while (i < N) loop  
   			if (a(i) >= 0 and b(i) /=1)  then 
				f:=0; 
			end if;

			if (a(i) < 0 and b(i) =1)  then 
				f:=0; 
			end if;
			i := i + 1;
		end loop; 

		pragma Assert(f = 1);

   end Main;


end Standard_running_true_unreach_call;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./Standard_running_true_unreach_call.gpr --prover=cvc4,z3,altergo --mode=all --report=all 
