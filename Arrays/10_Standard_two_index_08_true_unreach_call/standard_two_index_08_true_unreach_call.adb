with Ada.Text_IO; use Ada.Text_IO;

package body Standard_two_index_08_true_unreach_call with
  SPARK_Mode => on
is
   procedure Main is
		size: Integer:= 10;
   		a: array (0..size) of Integer;
   		b: array (0..size) of Integer;
		i: Integer:=0;
		j: Integer:=0;
   begin 


  		while( i < size ) loop 
			b(i) := 141665274;
			i := i+1; 
		end loop;

  		i := 1;
  		while( i < size ) loop 
			a(j) := b(i);
			i := i+8;
        	j := j+1; 
		end loop;

  		i := 1;
  		j := 0;

  		while( i < size ) loop 
			pragma Assert(a(j) = b(8*j+1));
        	i := i+8;
        	j := j+1;
  		end loop; 
       
   end Main;


end Standard_two_index_08_true_unreach_call;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./out_of_range.gpr --prover=cvc4,z3,altergo --mode=all --report=all
