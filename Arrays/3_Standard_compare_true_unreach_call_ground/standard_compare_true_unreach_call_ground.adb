with Ada.Text_IO; use Ada.Text_IO;

package body Standard_compare_true_unreach_call_ground with
  SPARK_Mode => on
is
    
   procedure Main is
		size: Integer:= 100000;
   		a: array (0..size) of Integer;
   		b: array (0..size) of Integer;
		i: Integer:=0;
		rv: Boolean:=true;
   begin  

  		while( i < size ) loop 
			if (a(i) /= b(i)) then
				rv:= false;  
			end if;
			i:= i+1;
		end loop;

  		if (rv) then 
			for x in Integer range 0..size-1 loop
				pragma Assert(a(x)=b(x));
			end loop;
		end if;
       
   end Main;


end Standard_compare_true_unreach_call_ground;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./standard_compare_true_unreach_call_ground.gpr -k -U --mode=all --report=all
