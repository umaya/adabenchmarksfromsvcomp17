with Ada.Text_IO; use Ada.Text_IO;

package body Standard_seq_init_true_unreach_call_ground with
  SPARK_Mode => on
is
   procedure Main is
		size: Integer:= 3;
   		a: array (0..size-1) of Integer; 
		i: Integer:=1; 
   begin   
		a(0) := 7;
		
		while( i < SIZE ) loop
			a(i) := a(i-1) + 1;
			i := i + 1;
		end loop;

		for x in Integer range 1..size-1 loop 
			pragma Assert(a(x) >= a(x-1));
		end loop; 


   end Main;


end Standard_seq_init_true_unreach_call_ground;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./Standard_seq_init_true_unreach_call_ground.gpr --prover=cvc4,z3,altergo --mode=all --report=all 
