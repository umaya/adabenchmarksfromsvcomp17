

package body Standard_strcpy_false_valid_deref_ground with
  SPARK_Mode => on
is
 

   procedure Main is
		N: Integer:= 100000;
   		src: array (0..N) of Integer;
   		dst: array (0..N) of Integer;
		i: Integer:=0;
   begin  
 
  		while (src(i) /= 0) loop
    		dst(i) := src(i);
    		i := i + 1;
		end loop; 
 
 		for  x in Integer range 0..i-1 loop
   			pragma Assert(dst(x) = src(x));
		end loop;
       
   end Main;


end Standard_strcpy_false_valid_deref_ground;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./standard_strcpy_false_valid_deref_ground.gpr -k -U  --mode=all --report=all
