with Ada.Text_IO; use Ada.Text_IO;

package body Standard_minInArray_true_unreach_call_ground with
  SPARK_Mode => on
is
   procedure Main is
		N: Integer:= 7;
   		a: array (0..N-1) of Integer;  
   		min: Integer:=0; 
   		i: Integer:=0; 
   begin   

		while (i < N) loop
			a(i):=-1; 
			i:= i+1;
		end loop;
   
		i:=0;
		while (i < N) loop
			if a(i) < min then 
				min := a(i);
			end if;
			i:= i+1;
		end loop;

 		for x in Integer range 0..N-1 loop 
			pragma Assert(a(x) >= min); 
		end loop;  
 
   end Main;


end Standard_minInArray_true_unreach_call_ground;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./Standard_minInArray_true_unreach_call_ground.gpr --prover=cvc4,z3,altergo --mode=all --report=all 
