with Ada.Text_IO; use Ada.Text_IO;

package body Standard_partition_true_unreach_call_ground with
  SPARK_Mode => on
is
   
   procedure Main is
		N: Integer:= 100000;
   		aa: array (0..N) of Integer;
   		bb: array (0..N) of Integer;
   		cc: array (0..N) of Integer;
		a: Integer:=0;
		b: Integer:=0;
		c: Integer:=0;
   begin 
 		 
  		while (a < N) loop
    		if (aa(a) >= 0) then 
		      bb(b) := aa(a);
		      b := b + 1;
   			end if;
			a := a + 1;  
		end loop;

		a := 0;
		while (a < N) loop
    		if (aa(a) < 0) then 
		      cc(c) := aa(a);
		      c := c + 1;
   			end if;
			a := a + 1;  
		end loop;
 
 		for  x in Integer range 0..b-1 loop
   			pragma Assert(bb(x) >= 0);
		end loop;
       
   end Main;


end Standard_partition_true_unreach_call_ground;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./out_of_range.gpr --prover=cvc4,z3,altergo --mode=all --report=all
