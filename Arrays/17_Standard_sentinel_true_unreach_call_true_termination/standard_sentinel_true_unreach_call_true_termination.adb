with Ada.Text_IO; use Ada.Text_IO;

package body Standard_sentinel_true_unreach_call_true_termination with
  SPARK_Mode => on
is
   procedure Main is
		N: Integer:= 7;
   		a: array (0..N-1) of Integer; 
		marker: Integer:=2; 
		pos: Integer:=3; 
		i : Integer:=0;
   begin   
 
   		if pos >= 0 and pos < N then
			a(pos):= marker;
		end if;
		
   		while (a(i) /= marker) loop 
			i := i + 1;
		end loop; 

		pragma Assert(i<= pos);

   end Main;


end Standard_sentinel_true_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./Standard_sentinel_true_unreach_call_true_termination.gpr --prover=cvc4,z3,altergo --mode=all --report=all 
