with Standard_sentinel_true_unreach_call_true_termination;
procedure Gmain with
  SPARK_Mode => on
is
begin
	Standard_sentinel_true_unreach_call_true_termination.Main; 
end Gmain;
