with Ada.Text_IO; use Ada.Text_IO;

package body Standard_all_diff2_false_unreach_call_ground with
  SPARK_Mode => on
is 
   procedure Main is
		size: Integer:= 10;
   		a: array (0..size) of Integer; 
		r: Boolean:=true;
   begin  

  		for i in Integer range 1..size-1 loop
			for j in Integer range 0..i-1 loop
				if (a(i)=a(j)) then
					r:=true;
				end if;
			end loop;
		end loop;

  		if (r) then
			for x in Integer range 0..size-1 loop
				for y in Integer range x+1..size-1 loop
					pragma Assert(a(x)/=a(y));
				end loop;	
			end loop;
		end if;
        
   end Main;

end Standard_all_diff2_false_unreach_call_ground;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./standard_all_diff2_false_unreach_call_ground.gpr --mode=all --report=all -U -k
