with Standard_vararg_true_unreach_call_ground_true_termination;
procedure Gmain with
  SPARK_Mode => on
is
begin
	Standard_vararg_true_unreach_call_ground_true_termination.Main; 
end Gmain;
