with Ada.Text_IO; use Ada.Text_IO;

package body Standard_vararg_true_unreach_call_ground_true_termination with
  SPARK_Mode => on
is
   procedure Main is
		size: Integer:= 3;
   		aa: array (0..size-1) of Integer:=(1,1,1); 
		a: Integer:=0; 
   begin   

		for x in Integer range 0..size-1 loop 
			aa(x):=2;
		end loop; 
 
		for x in Integer range 0..size-1 loop 
			if aa(x) >= 0 then 
				a:= a+1;
			end if;
		end loop; 
	 

		for x in Integer range 0..a-1 loop 
			pragma Assert(aa(x)>=0);
		end loop; 


   end Main;


end Standard_vararg_true_unreach_call_ground_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./Standard_vararg_true_unreach_call_ground_true_termination.gpr --prover=cvc4,z3,altergo --mode=all --report=all
--NOTE: This Ada program is slightly different from the version Standard_vararg_true_unreach_call_ground_true_termination in C. 
