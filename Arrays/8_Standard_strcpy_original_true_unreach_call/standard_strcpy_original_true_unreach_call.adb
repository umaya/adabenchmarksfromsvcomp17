with Ada.Text_IO; use Ada.Text_IO;

package body Standard_strcpy_original_true_unreach_call with
  SPARK_Mode => on
is
 
   procedure Main is
		N: Integer:= 100000;
   		src: array (0..N) of Integer;
   		dst: array (0..N) of Integer;
		i: Integer:=0;
		j: Integer:=0; 
   begin 
 

  		while( j < N ) loop 
			src(j) := -81815168;
			j := j+1; 
		end loop;
 		
  		while (i < N and src(i) /= 0) loop
    		dst(i) := src(i);
    		i := i + 1;
		end loop; 
 
		i := 0;
 		while (i < N and src(i) /= 0) loop
   			pragma Assert(dst(i) = src(i));
		end loop;
       
   end Main;


end Standard_strcpy_original_true_unreach_call;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./standard_strcpy_original_true_unreach_call.gpr -k -U --mode=all --report=all
