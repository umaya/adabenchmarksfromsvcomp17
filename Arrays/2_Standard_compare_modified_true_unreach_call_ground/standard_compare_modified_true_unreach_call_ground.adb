with Ada.Text_IO; use Ada.Text_IO;

package body Standard_compare_modified_true_unreach_call_ground with
  SPARK_Mode => on
is
    
   procedure Main is
		size: Integer:= 10;
   		a: array (0..size) of Integer;
   		b: array (0..size) of Integer;
   		c: array (0..size) of Integer;
		i: Integer:=0;
		rv: Boolean:=true;
   begin  

  		while( i < size ) loop 
			if (a(i) /= b(i)) then
				rv:= false;
			end if;
			c(i) := a(i);
			i:= i+1;
		end loop;

  		if (rv) then
			for x in Integer range 0..size-1 loop
				pragma Assert(a(x)=b(x));
			end loop;
		end if;
       
		for x in Integer range 0..size-1 loop 
			pragma Assert(a(x)=c(x));
		end loop;
   end Main;


end Standard_compare_modified_true_unreach_call_ground;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./standard_compare_modified_true_unreach_call_ground.gpr  --mode=all --report=all -U -k
