with Ada.Text_IO; use Ada.Text_IO;
package body Float_under_1 with 
	SPARK_MODE => on
is
	procedure Subtraction is
		B: Float:= 0.0;
	begin 
		while B>-10.0 loop
			pragma Assert (B> -33700000000000000000000000000000000000.0);
			B:= B-1.0;
		end loop;
	end Subtraction;
end Float_under_1;


--COMMAND LINE: gnatprove -f -P ./float_underflow.gpr --mode=all --report=all

