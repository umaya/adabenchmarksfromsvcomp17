package body Div_by_zero_1  
 with
  SPARK_Mode => on
is
 
   Arr: array (1..10) of Integer;
   G1, G2, G3 : Integer := 0;  
 
   procedure Check_OK (OK : Boolean; K: Integer) is
   begin 
	
      
	 Arr(G3) := 1; 	 
	 G2 := Arr(G3+100); 	 
	 pragma Assert(G1/=0);
         G1 := 5/G1;		 
   end Check_OK;


end Div_by_zero_1;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./out_of_range_2.gpr -U -k --mode=all --report=all
