package body Out_of_range_2 with
  SPARK_Mode => on
is
 
   Arr: array (1..10) of Integer;
   G1, G2, G3 : Integer := 0; 
 
   procedure Check_OK (OK : Boolean; K: Integer) is
   begin 
	
      if OK then  
		 G3 := G3 +1; 		 
		 pragma Assert(G3>10);
		 Arr(G3) := 1; 		 
      end if; 
   end Check_OK;

end Out_of_range_2;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./out_of_range_3.gpr -U -k --mode=all --report=all
