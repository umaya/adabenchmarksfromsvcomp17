package body Div_by_zero_2 
 with
  SPARK_Mode => on
is
 
   G1, G2, G3 : Integer := 0; 

   procedure Check_OK (OK : Boolean; K: Integer) is
   begin 
	 G2 := 1-K; 	 
	 pragma Assert(G2/=0);
         G1 := 5/G2;		   
   end Check_OK;


end Div_by_zero_2;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./out_of_range_4.gpr -U -k --mode=all --report=all
