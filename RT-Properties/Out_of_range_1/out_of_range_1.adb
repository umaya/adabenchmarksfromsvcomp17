with Ada.Text_IO; use Ada.Text_IO;

package body Out_of_range_1 with
	SPARK_MODE => on
is
	procedure Out_of_range is 
		arr: array (1..5) of Integer;
		D	: Integer:= 1;
	begin  
		arr(1):=0;   
		while D <2 loop 
			D:= D+1;
		    pragma Assert(D<=5);
			arr(D):= D;  
		end loop;
	end Out_of_range;
end Out_of_range_1;
 
-- COMMAND LINE: gnatprove -f -P ./out_of_range_1.gpr -U -k --mode=all --report=all

