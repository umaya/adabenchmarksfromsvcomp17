with Ada.Text_IO; use Ada.Text_IO;

package body Int_under_1 with 
	SPARK_MODE => on
is
 

	procedure Integer_Sub is
		B:  Integer:=0;
	begin 
		while B > -10 loop
			pragma Assert(B>-2147483648);
			B:= B-1;
		end loop;
	end Integer_Sub;
end Int_under_1;


--COMMAND LINE:  gnatprove -f -P ./int_underflow.gpr --mode=all --report=all -U -k

