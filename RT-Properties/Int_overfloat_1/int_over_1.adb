with Ada.Text_IO; use Ada.Text_IO;

package body Int_over_1 with 
	SPARK_MODE => on
is

	procedure Integer_Sum is
		A: Integer:= 400;
	begin 
		while A < 10 loop 
     		pragma Assert(A<2147483647);
			A:= A+1;
		end loop;
	end Integer_Sum; 
end Int_over_1;


--COMMAND LINE:  gnatprove -f -P ./int_overflow.gpr --mode=all --report=all -U -k

