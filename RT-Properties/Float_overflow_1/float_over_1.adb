with Ada.Text_IO; use Ada.Text_IO;
package body Float_over_1 with 
	SPARK_MODE => on
is
	procedure Sum is
		A: Float:= 1.34;
	begin 
		while A < 10.0 loop
			pragma Assert (A < 33700000000000000000000000000000000000.0); 
			A:= A+1.0; 
		end loop; 
	end Sum;
end Float_over_1;


--COMMAND LINE: gnatprove -f -P ./float_overflow.gpr --mode=all --report=all

