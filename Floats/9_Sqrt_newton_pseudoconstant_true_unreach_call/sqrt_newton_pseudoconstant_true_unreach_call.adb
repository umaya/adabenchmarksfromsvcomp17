with Ada.Text_IO; use Ada.Text_IO;
package body Sqrt_newton_pseudoconstant_true_unreach_call with
  SPARK_Mode => on
is

   function sqrt (input: Float) return Float is
		xn, xnp1, residu, lsup, linf: Float:=0.0;	
		i: Integer:=0;
		cond: Boolean;
		EPS: Float:= 0.000001;
   begin
		if (input <= 1.0) then 
			xn := 1.0; 
		else 
			xn := 1.0/input; 
		end if;

		xnp1 := xn;
		residu := 2.0*EPS*(xn+xnp1);
  		lsup := EPS * (xn+xnp1);
  		linf := -lsup;
  		if ((residu > lsup) or (residu < linf)) then
			cond :=  true;
		else 
			cond := false;
		end if;
		while (cond) loop
			xnp1 := xn + xn*(1.0-input*xn*xn) / 2.0;
		    residu := 2.0*(xnp1-xn);
    		xn := xnp1;
    		lsup := EPS * (xn+xnp1);
    		linf := -lsup;
     		if ((residu > lsup) or (residu < linf)) then
				cond :=  true;
			else 
				cond := false;
			end if;
			i:= i+1;
		end loop;
		return 1.0 / xnp1; 
   end sqrt;

   procedure Main is
		dd, r: Float;
		epsilon: Float:= 0.00000001;
		eplus, eminus: Float;
   begin 
 		for d in Integer range  0..19 loop
			dd := 0.151985; 
			eplus:= dd+epsilon;
			eminus:= dd-epsilon;
			pragma Assert(dd >= eplus and dd <= eminus);
			r := eplus;
			pragma Assert(r >= 0.9 and r <= 5.0);
		end loop;
   end Main;


end Sqrt_newton_pseudoconstant_true_unreach_call;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./sqrt_newton_pseudoconstant_true_unreach_call.gpr --prover=cvc4,z3,altergo --mode=all --report=all
