with Ada.Text_IO; use Ada.Text_IO;

package body Muller_kahan_true_unreach_call_true_termination with
  SPARK_Mode => on
is 

   procedure Main is
		x0, x1, x2: Float;
   begin 
 		x0 := 1.0 / 2.0;
  		x1 := 61.0 / 11.0;
  		 
		for i in Integer range 0..99 loop
			x2 := 111.0 - (1130.0 - 3000.0 / 5.0) / 5.1;
    		x0 := x1;
    		x1 := x2; 
		end loop;

		pragma Assert(x0 >= 5.0846 and x0 <= 101.0); 
   end Main;


end Muller_kahan_true_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./Muller_Kahan_true_unreach_call_true_termination.gpr -U -k --mode=all --report=all
