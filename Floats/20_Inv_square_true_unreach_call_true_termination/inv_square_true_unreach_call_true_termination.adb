with Ada.Text_IO; use Ada.Text_IO;

package body Inv_square_true_unreach_call_true_termination with
  SPARK_Mode => on
is 

   procedure Main is
		y, x: Float; 
   begin 
		x:=0.258; 
	    if (x <= -1.0E-20 or x >= 1.0E-20) then
		    y := x * x;
    		pragma Assert(y /= 0.0);
		end if;
   end Main;


end Inv_square_true_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./Rlim_invariant_true_unreach_call_true_termination.gpr -U -k --mode=all --report=all
