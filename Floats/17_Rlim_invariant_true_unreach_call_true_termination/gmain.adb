with Rlim_invariant_true_unreach_call_true_termination;
procedure Gmain with
  SPARK_Mode => on
is
begin
	Rlim_invariant_true_unreach_call_true_termination.Main; 
end Gmain;
