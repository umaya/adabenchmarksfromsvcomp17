with Ada.Text_IO; use Ada.Text_IO;

package body Rlim_invariant_true_unreach_call_true_termination with
  SPARK_Mode => on
is 

   procedure Main is
		X, Y, S, R, D: Float:=12.0; 
   begin 
		Y:=0.0; 
	    for i in Integer range 0..9 loop 
			X:= 95874.5881;
			D:= 8519.87; 
 
			S := Y; 
    		Y := X;
    		R := X - S;
    		if R <= (-D)  then 
				Y := S - D;
			else if R >= D then
					Y := S + D;
				 end if;
			end if; 
			pragma Assert(Y >= -129.0 and Y <= 129.0);
		end loop;
   end Main;


end Rlim_invariant_true_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./Rlim_invariant_true_unreach_call_true_termination.gpr -U -k --mode=all --report=all
