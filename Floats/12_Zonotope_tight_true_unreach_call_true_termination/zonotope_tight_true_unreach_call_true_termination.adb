with Ada.Text_IO; use Ada.Text_IO;

package body Zonotope_tight_true_unreach_call_true_termination with
  SPARK_Mode => on
is 

   procedure Main is
		x: Float:= 8.359254;
		y: Float:= 0.0;
   begin 
 		y := (x*x) - x;
	    if y >= 0.0 then 
			y := x / 10.0;
		else 
	        y := x*x + 2.0;
		end if;
		pragma Assert(y >= 0.0158 and y <= 4.0); 
   end Main;


end Zonotope_tight_true_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./Zonotope_tight_true_unreach_call_true_termination.gpr -U -k --mode=all --report=all
