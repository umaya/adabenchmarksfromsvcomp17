with Ada.Text_IO; use Ada.Text_IO;

package body Loop_true_unreach_call with
  SPARK_Mode => on
is
   
   procedure Main is
		x, y, z: Float;
   begin 
 		x := 1.0;
  		y := 10000000.0;
		z := 42.0;
        
		while (x < y) loop
		    x := x + 1.0;
    		y := y - 1.0;
    		z := z + 1.0;
		end loop; 
		pragma Assert(z >= 0.0 and z <= 100000000.0);
   end Main;


end Loop_true_unreach_call;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./loop_true_unreach_call.gpr -U -k --mode=all --report=all
