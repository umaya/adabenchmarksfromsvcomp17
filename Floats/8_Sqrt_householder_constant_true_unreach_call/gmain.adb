with Sqrt_householder_constant_true_unreach_call;
procedure Gmain with
  SPARK_Mode => on
is
begin
	Sqrt_householder_constant_true_unreach_call.Main; 
end Gmain;
