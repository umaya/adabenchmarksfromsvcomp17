with Ada.Text_IO; use Ada.Text_IO;

package body Water_pid_true_unreach_call_true_termination with
  SPARK_Mode => on
is
   function y (i: Float) return Float is
		yn: Float:=0.0;	
   begin
		yn := yn + i;
		return yn;
   end y;

   procedure Main is
		ui, yi, yc, K, T, taui, taud, ei, sumej, epi: Float;
   begin 
 		T := 1.0;
  		taui := 1.0;
  		taud := 1.0;
  		K := 0.5;
		yc := 0.5;
  		yi := y(0.5);
		epi := yc-yi;
		sumej := epi;
		for i in Integer range 0..119 loop
			yi := y(0.1);
    		ei := yc-yi;
    		sumej := sumej+ei;
    		ui := K*(ei+sumej*T/taui+taud/T*(ei-epi));
    		epi := ei; 
            pragma Assert(epi >= -1.0 and epi <= 1.0);
		end loop;
   end Main;


end Water_pid_true_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./water_pid_true_unreach_call_true_termination.gpr -U -k --mode=all --report=all
