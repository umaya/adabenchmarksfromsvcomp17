with Rlim_invariant_true_unreach_call_true_termination_version2;
procedure Gmain with
  SPARK_Mode => on
is
begin
	Rlim_invariant_true_unreach_call_true_termination_version2.Main; 
end Gmain;
