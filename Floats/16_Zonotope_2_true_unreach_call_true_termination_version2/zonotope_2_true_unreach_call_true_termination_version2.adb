with Ada.Text_IO; use Ada.Text_IO;

package body Zonotope_2_true_unreach_call_true_termination_version2 with
  SPARK_Mode => on
is 

   procedure Main is
		x: Float:=12.0;
		x1: Float:=12.0;
		y: Float:=16.0;
		y1: Float:=16.0;
   begin 
		 
	    for i in Integer range 0..9 loop 
			x := x1;
		    y := y1;
    		x1 := 3.0 * x / 4.0 + y / 4.0;
			y1 := x / 4.0 + 3.0 * y / 4.0;
 
			pragma Assert(x1 >= 0.11 and x1 <= 100.11);
			pragma Assert(y1 >= 0.11 and y1 <= 100.11);
		end loop;
   end Main;


end Zonotope_2_true_unreach_call_true_termination_version2;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./Zonotope_2_true_unreach_call_true_termination.gpr -U -k --mode=all --report=all
