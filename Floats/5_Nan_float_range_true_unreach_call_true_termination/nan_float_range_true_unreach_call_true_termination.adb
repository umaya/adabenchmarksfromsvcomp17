with Ada.Text_IO; use Ada.Text_IO;

package body Nan_float_range_true_unreach_call_true_termination with
  SPARK_Mode => on
is 
 
   procedure Main is
		x: Float;
   begin 
 		x :=  10.846216;
		if (x >= -10000000000.0 and x <= 10000000000.0) then
			pragma Assert(x=x);
		end if;
   end Main;


end Nan_float_range_true_unreach_call_true_termination;
 

 
