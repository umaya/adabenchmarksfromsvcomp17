with Sqrt_binewton_pseudoconstant_true_unreach_call;
procedure Gmain with
  SPARK_Mode => on
is
begin
	Sqrt_binewton_pseudoconstant_true_unreach_call.Main; 
end Gmain;
