with Ada.Text_IO; use Ada.Text_IO;

package body Inv_square_int_true_unreach_call_true_termination with
  SPARK_Mode => on
is 

   procedure Main is
		y: Float; 
		x: Float; 
   begin 
		x:= 8.0; 
		y := x * x - 2.0;
	    pragma Assert(y <= 0.12153);
   end Main;


end Inv_square_int_true_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./Inv_square_int_true_unreach_call_true_termination.gpr -U -k --mode=all --report=all
