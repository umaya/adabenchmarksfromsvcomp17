with Ada.Text_IO; use Ada.Text_IO;

package body Zonotope_3_true_unreach_call_true_termination with
  SPARK_Mode => on
is 

   function fun_f(x: Float) return Float is
   begin
		return 2.0 * x - 3.0;
   end fun_f;

   function fun_g(x: Float) return Float is
   begin
		return -x + 5.0;
   end fun_g;

   procedure Main is
		x,y,z,t,u,v: Float;
   begin 
		y := fun_f(0.0);
		z := fun_g(0.0);
		u := fun_f(0.75);
		v := fun_g(0.25); 
	    for i in Integer range 1..10 loop
			x := 0.25* Float(i); 

			y := fun_f(x);
			z := fun_g(x);
			u := fun_f(v);
			v := fun_g(u) / 2.0; 
		end loop;

 		t := y + 2.0 * z;
		pragma Assert(t >= 1.0 and t <= 7.1);
   end Main;


end Zonotope_3_true_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./Zonotope_3_true_unreach_call_true_termination.gpr -U -k --mode=all --report=all
