with Ada.Text_IO; use Ada.Text_IO;

package body Nan_float_false_unreach_call_true_termination with
  SPARK_Mode => on
is 

   procedure Main is
		x: Float;
   begin 
 		x :=  0.453681;  
		pragma Assert(x=x);
   end Main;


end Nan_float_false_unreach_call_true_termination;
 

 
