with Nan_float_false_unreach_call_true_termination;
procedure Gmain with
  SPARK_Mode => on
is
begin
	Nan_float_false_unreach_call_true_termination.Main; 
end Gmain;
