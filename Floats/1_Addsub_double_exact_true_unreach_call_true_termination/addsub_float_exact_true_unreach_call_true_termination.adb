with Ada.Text_IO; use Ada.Text_IO;

package body Addsub_float_exact_true_unreach_call_true_termination with
  SPARK_Mode => on
is
   
   procedure Main is
		x, y, z, r: Float;
   begin 
 		x := 10000000.0; 
		y := x + 1.0;
		z := x - 1.0;
		r :=  y -z ; 

		pragma Assert(r = 2.0);
   end Main;


end Addsub_float_exact_true_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./addsub_float_exact_true_unreach_call_true_termination.gpr -k -U --mode=all --report=all
