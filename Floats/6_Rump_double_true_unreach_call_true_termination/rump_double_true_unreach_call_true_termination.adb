with Ada.Text_IO; use Ada.Text_IO;

package body Rump_double_true_unreach_call_true_termination with
  SPARK_Mode => on
is
   
   procedure Main is
		x, y, r: Float;
   begin 
 		x := 77617.0;
  		y := 33096.0;
		r := 333.75 * y*y*y*y*y*y + x*x * (11.0 * x*x*y*y - y*y*y*y*y*y - 121.0 * y*y*y*y - 2.0) + 5.5 * y*y*y*y*y*y*y*y + x / (2.0 * y);
        if (r >= 0.0) then
			Put_Line("si" & Float'Image(r));
		end if;
		pragma Assert(r >= 0.0);
   end Main;


end Rump_double_true_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./rump_double_true_unreach_call_true_termination.gpr --prover=cvc4,z3,altergo --mode=all --report=all
