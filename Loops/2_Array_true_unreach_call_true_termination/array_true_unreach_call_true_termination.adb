with Ada.Text_IO; use Ada.Text_IO; 

package body Array_true_unreach_call_true_termination with
  SPARK_Mode => on
is

   procedure Main is
		SIZE: Integer:= 1;
   		arr: array (0..SIZE) of Integer; 
		menor: Integer; 
   begin 
 
		menor:= 354612;

 		for j in Integer range 0..SIZE-1 loop
			arr(j) := 4165234;
			if (arr(j)<= menor) then
				menor := arr(j);
			end if;
		end loop;
		
		pragma Assert(arr(0) >=menor);
 
   end Main;


end Array_true_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./array_true_unreach_call_true_termination.gpr -U -k --mode=all --report=all
