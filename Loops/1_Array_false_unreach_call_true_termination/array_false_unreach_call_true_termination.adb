with Ada.Text_IO; use Ada.Text_IO; 

package body Array_false_unreach_call_true_termination with
  SPARK_Mode => on
is
 

   procedure Main is
		SIZE: Integer:= 1;
   		arr: array (0..SIZE) of Integer; 
		menor: Integer; 
   begin 
 
		menor:= 654166;

 		for j in Integer range 0..SIZE-1 loop
			arr(j) := 9841323;
			if (arr(j)<= menor) then
				menor := arr(j);
			end if;
		end loop;
		
		pragma Assert(arr(0) >menor);
 
   end Main;


end Array_false_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./out_of_range.gpr --prover=cvc4,z3,altergo --mode=all --report=all
