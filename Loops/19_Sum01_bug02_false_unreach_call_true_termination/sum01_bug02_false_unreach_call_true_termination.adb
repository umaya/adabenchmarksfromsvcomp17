with Ada.Text_IO; use Ada.Text_IO; 

package body Sum01_bug02_false_unreach_call_true_termination with
  SPARK_Mode => on
is   

   procedure Main is   
	   n: Integer:= 10;
	   j: Integer:= 35;
	   sn : Integer:= 0;
	   a: Integer:= 5;
   begin 
		for i in Integer range 1..n loop
			sn := sn + a; 
 			if i<j then 
				sn:= sn-10;
			end if;
			j:= j-1; 
		end loop;

		pragma Assert(sn= n*a);  
   end Main;


end Sum01_bug02_false_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./Sum01_bug02_false_unreach_call_true_termination.gpr -U -k --mode=all --report=all
