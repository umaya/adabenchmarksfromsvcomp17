with Ada.Text_IO; use Ada.Text_IO; 

package body Count_up_down_true_unreach_call_true_termination with
  SPARK_Mode => on
is

   procedure Main is
		x, y, n: Integer;  
   begin 
 
		n:= 9848631;
	
		if (n<0) then 
			n := n*(-1);
		end if;

		x:=n;
		y:=0;
 
		while (x>0) loop
			x:= x-1;
			y:= y+1;
		end loop;
  
		pragma Assert(y=n);
 
   end Main;


end Count_up_down_true_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./count_up_down_true_unreach_call_true_termination.gpr -U -k --mode=all --report=all
