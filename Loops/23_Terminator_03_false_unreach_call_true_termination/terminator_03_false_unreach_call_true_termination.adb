with Ada.Text_IO; use Ada.Text_IO;

package body Terminator_03_false_unreach_call_true_termination with
  SPARK_Mode => on
is 

   procedure Main is
		x: Integer:=32; 
		y: Integer:=125;  
   begin 
		if (y>0) then 
			while (x<100) loop
				x:= x+y; 
			end loop; 
		end if;
		pragma Assert(y<0 and x>=100); 
   end Main;


end Terminator_03_false_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./Terminator_03_false_unreach_call_true_termination.gpr -U -k --mode=all --report=all
