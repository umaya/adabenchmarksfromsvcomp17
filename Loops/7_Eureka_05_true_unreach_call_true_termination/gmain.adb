with Eureka_05_true_unreach_call_true_termination;
procedure Gmain with
  SPARK_Mode => on
is
begin
	Eureka_05_true_unreach_call_true_termination.SelectionSort;
    Eureka_05_true_unreach_call_true_termination.Main; 
end Gmain;
