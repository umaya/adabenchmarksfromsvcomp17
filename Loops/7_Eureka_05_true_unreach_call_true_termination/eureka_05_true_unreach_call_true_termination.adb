with Ada.Text_IO; use Ada.Text_IO; 

package body Eureka_05_true_unreach_call_true_termination with
  SPARK_Mode => on
is
    SIZE: Integer:= 5;  
	arr, arr2: array(0..SIZE-1) of Integer; 
   	n : Integer:=SIZE;

	procedure SelectionSort is
		rh, temp: Integer ;
	begin
		for lh in Integer range 0..n-1 loop
			rh := lh;
			for i in Integer range 0..n-1 loop
				if (arr(i) < arr(rh)) then
					rh := i;
				end if;
			end loop;
			temp := arr(lh);
      		arr(lh) := arr(rh);
			arr(rh) := temp;
		end loop;
	end SelectionSort;

   procedure Main is
		
   begin 
	for i in Integer range 0..SIZE-1 loop
		arr2(i):=i;
	end loop;

	SelectionSort;

	for i in Integer range 0..SIZE-1 loop 
		pragma Assert(arr2(i)=i);
	end loop;
 
   end Main;


end Eureka_05_true_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./eureka_05_true_unreach_call_true_termination.gpr -k -U --mode=all --report=all
