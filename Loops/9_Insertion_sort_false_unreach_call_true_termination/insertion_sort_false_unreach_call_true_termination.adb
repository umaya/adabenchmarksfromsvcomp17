with Ada.Text_IO; use Ada.Text_IO; 
package body Insertion_sort_false_unreach_call_true_termination with
  SPARK_Mode => on
is 

   procedure Main is
		SIZE: Integer:= 3;  
		v: array(0..SIZE-1) of Integer; 
		key, i : Integer;
   begin 
 
		for j in Integer range 0..SIZE-1 loop
			key	:= v(j);
			i := j-1;
			while ((i>0) and (v(i)>key)) loop
				if (i<2) then
					v(i+1) := v(i);
					i := i - 1;
				end if;
			end loop;
			v(i+1) := key; 
		end loop;
	 
		for k in Integer range 0..SIZE-1 loop
			pragma Assert(v(k-1)<=v(k));
		end loop;
 
   end Main;


end Insertion_sort_false_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./insertion_sort_false_unreach_call_true_termination.gpr -U -k--mode=all --report=all
