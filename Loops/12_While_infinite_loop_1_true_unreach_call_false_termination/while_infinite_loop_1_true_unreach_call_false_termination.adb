with Ada.Text_IO; use Ada.Text_IO; 

package body While_infinite_loop_1_true_unreach_call_false_termination with
  SPARK_Mode => on
is   

   procedure Main is  
	   x: Integer:=0;
   begin 
 
		while (true) loop 
			pragma Assert(x=0); 
		end loop;  
   end Main;


end While_infinite_loop_1_true_unreach_call_false_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./While_infinite_loop_3_true_unreach_call_false_termination.gpr -U -k --mode=all --report=all
