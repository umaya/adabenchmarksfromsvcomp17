with Ada.Text_IO; use Ada.Text_IO;

package body Terminator_02_true_unreach_call_true_termination with
  SPARK_Mode => on
is 

   procedure Main is
		x: Integer:=32; 
		z: Integer:=125; 
		tmp: Boolean:=true; 
   begin 
		while (x<100 and z>100) loop
			if (tmp=true) then
				x := x + 1;
			else 
				x:=x-1;
				z:=z-1;
			end if;
		end loop; 
		pragma Assert(x>=100);
		pragma Assert(z<=100);
   end Main;


end Terminator_02_true_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./Terminator_02_true_unreach_call_true_termination.gpr -U -k --mode=all --report=all
