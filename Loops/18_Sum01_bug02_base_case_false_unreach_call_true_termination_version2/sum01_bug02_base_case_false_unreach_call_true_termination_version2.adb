with Ada.Text_IO; use Ada.Text_IO; 

package body Sum01_bug02_base_case_false_unreach_call_true_termination_version2 with
  SPARK_Mode => on
is   

   procedure Main is   
	   x: Integer:= 10;
	   sn : Integer:= 0;
	   a: Integer:= 5;
   begin 
		for i in Integer range 1..x loop
			sn := sn + a; 
 			if i=4 then 
				sn:= sn-10;
			end if; 
		end loop;

		pragma Assert(sn< x*a);  
   end Main;


end Sum01_bug02_base_case_false_unreach_call_true_termination_version2;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./Sum01_bug02_base_case_false_unreach_call_true_termination_version2.gpr -U -k --mode=all --report=all
