with Ada.Text_IO; use Ada.Text_IO; 

package body Sum03_true_unreach_call_true_termination with
  SPARK_Mode => on
is   

   procedure Main is   
	   x, sn : Integer:= 0;
	   a: Integer:= 5;
   begin 
		for i in Integer range 1..10 loop
			sn := sn + a;
			x:= x + 1;
			pragma Assert(sn =x*a);  
		end loop;
   end Main;


end Sum03_true_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./Sum03_true_unreach_call_true_termination.gpr -U -k --mode=all --report=all
