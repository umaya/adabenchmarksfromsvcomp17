with Ada.Text_IO; use Ada.Text_IO; 

package body Eureka_01_true_unreach_call with
  SPARK_Mode => on
is 
   function Main return Integer is
		INFINITY: Integer:= 899;  
		nodecount, edgecount, src, y, x: Integer;
		Source : array(0..19) of Integer:= (0,4,1,1,0,0,1,3,4,4,2,2,3,0,0,3,1,2,2,3);
		Dest: array(0..19) of Integer:= (1,3,4,1,1,4,3,4,3,0,0,0,0,2,3,0,2,1,0,4);
		Weight: array(0..19) of Integer:= (0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19);
		distance: array(0..4) of Integer;
   
   begin 

 		src:=0;
		nodecount := 9847984;
		edgecount := -98498493;
	
		if (not(0 <= nodecount and nodecount <= 4)) then 
			return 0;
		end if;

		if (not(0 <= edgecount and edgecount <= 19)) then 
			return 0;
		end if;
 
		for i in Integer range 0..nodecount-1 loop
			if (i = src) then
				distance(i) := 0;
			else
				distance(i) := INFINITY; 
			end if;
		end loop;
  
 
		for i in Integer range 0..nodecount-1 loop
      		for j in Integer range 0..edgecount-1 loop
	 			x := Dest(j);
	  			y := Source(j);
			    if (distance(x) > distance(y) + Weight(j)) then
	      			distance(x) := distance(y) + Weight(j);
	            end if;
			end loop;
		end loop; 

		for j in Integer range 0..edgecount-1 loop
 			x := Dest(j);
  			y := Source(j);
		    if (distance(x) > distance(y) + Weight(j)) then
      			return 0;
            end if;
		end loop;

		for i in Integer range 0..nodecount-1 loop
			pragma Assert(distance(i)>=0);
		end loop;

		return 0;
 
   end Main;


end Eureka_01_true_unreach_call;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./out_of_range.gpr --prover=cvc4,z3,altergo --mode=all --report=all
