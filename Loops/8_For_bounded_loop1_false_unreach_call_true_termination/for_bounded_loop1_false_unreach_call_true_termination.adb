with Ada.Text_IO; use Ada.Text_IO; 

package body For_bounded_loop1_false_unreach_call_true_termination with
  SPARK_Mode => on
is 

   function Main return Integer is
		x, y, n : Integer:=0; 
   begin 
 
		n:= -9849822;
		if (not(n>0)) then 
			return 0;
		end if;

 		for i in Integer range 0..n-1 loop
			x := x -y;
			pragma Assert(x=0);
			y:= 4554654;

			if (not(y/=0)) then
				return 0;
			end if;
			
			x:= x +y;
			pragma Assert(x/=0);
		end loop;
		
 		return 0;
 
   end Main;


end For_bounded_loop1_false_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./for_bounded_loop1_false_unreach_call_true_termination.gpr -U -k --mode=all --report=all
