with Ada.Text_IO; use Ada.Text_IO; 

package body Sum04_true_unreach_call_true_termination with
  SPARK_Mode => on
is   

   procedure Main is  
	   SIZE: Integer:= 8;
	   i, sn : Integer:= 0;
	   a: Integer:= 5;
   begin 
 		for i in Integer range 1..SIZE loop
    		sn := sn + a;
		end loop;

		pragma Assert(sn = SIZE*a or sn = 0);  
   end Main;


end Sum04_true_unreach_call_true_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./Sum04_true_unreach_call_true_termination.gpr -U -k --mode=all --report=all
