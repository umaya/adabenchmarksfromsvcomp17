with Ada.Text_IO; use Ada.Text_IO; 

package body While_infinite_loop_3_true_unreach_call_false_termination with
  SPARK_Mode => on
is  

   x: Integer:=0;

   

   function eval (y: Integer)  return Integer is
   begin	
	 	return y+1;
   end eval;

   procedure Main is 
	i: Integer:=0; 
   begin 
 
		while (true) loop
			x:= eval(i);
			pragma Assert(x>0);
			i:= i+1;
		end loop;  
   end Main;


end While_infinite_loop_3_true_unreach_call_false_termination;
 

--COMMAND LINE EXECUTED = gnatprove -f -P ./While_infinite_loop_3_true_unreach_call_false_termination.gpr -U -k --mode=all --report=all
